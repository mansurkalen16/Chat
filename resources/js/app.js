require('./bootstrap');

import Vue from 'vue';
import Chat from './components/Chat.vue';


const app = new Vue({
    el: '#app',
    components: {
        'chat': Chat
    }
});
