<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function getMessages(){
        $messages = Message::pluck('message');
        return $messages;
    }

    public function store(Request $request){
        $message = new Message();
        $post = $request->message;
        $message->message = $post;
        $message->save();

        return $post;
    }
}
